# Pickup Object Player Followed

This asset has the prefab in VRChat world made with udon.

このAssetはVRChatのUDONによって作成されたワールドにおいてプレイヤーに追従するオブジェクトです。

Players can toggle the prefab follow/unfollow.

追従の有無を切り替えることが出来ます。


The asset include the tablet prefab for [iwaSync3 Video Player](https://hoshinolabs.booth.pm/items/2666275).

またiwaSync3 Video Player向けのプレイヤーに追従するPrefabも同梱しています。

you can try the asset in the links.

以下のlink先からこのアセットを試用できます。

https://vrchat.com/home/world/wrld_9d717036-b06c-4e73-9e79-c15a09be379d

## Download link

Unity Package is [here](https://gitlab.com/makihiro3/pickup-object-follow-player/-/releases).

## How to use/使用方法

1. Import the latest VRCSDK3-World to Unity. / 最新のVRCSDK3-WorldをUnityにインポートします。
2. Import [UdonSharp](https://github.com/MerlinVR/UdonSharp) above v0.19.2. / v0.19.2以降のUdonSharpをインポートします。
3. (Optional) Import [iwaSync3 Video Player](https://hoshinolabs.booth.pm/items/2666275) if you use iwaSync Followed Tablet. / iwaSync Followed Tabletを利用する場合はiwaSync3 Video Playerをインポートします。
4. Import this unitypackage. / このunitypackageをインポートします。
