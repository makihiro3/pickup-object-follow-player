﻿// SPDX-License-Identifier: MIT
using UnityEngine;

namespace makihiro.PickupObjectFollowPlayer.Debug
{
    public class Follower : MonoBehaviour
    {
        [SerializeField] public Transform target;

        private Vector3 dPos;

        private Quaternion dRot;

        private void Start()
        {
            var t = transform;
            var rot = Quaternion.Inverse(target.rotation);
            dPos = rot * (t.position - target.position);
            dRot = t.rotation * rot;
        }

        private void Update()
        {
            var rot = target.rotation;
            var t = transform;
            t.rotation = rot * dRot;
            t.position = rot * dPos + target.position;
        }
    }
}